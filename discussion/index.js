//Mini-activity:

/*function displayMsgToSelf(message){
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
	console.log(message);
}

displayMsgToSelf("Hello past self!");*/

/*function displayMsgToSelf(message){
	console.log(message);
}

displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");
displayMsgToSelf("Hello past self!");*/



function displayMsgToSelf(message){
	for (let i = 0; i < 10; i++){
		console.log(message);
	}
}

displayMsgToSelf("Hello past me");

// [Try-Catch-Finally Statement]
/*
	- "try catsh" statements are commonly used for error handling
	- there are instances when the application returns on error/warning that is not necessarily an error in the context of our code.
	- these errors are a result of an attempt of the programming language to help develope in creating efficient code.
	-they are used to specify a response whenever an exception/error is received.
	-it is also useful for debugging code because of the "error" object that can ve "caught" when using the try catch statements.
	-the "finally" block is used to specify a response/action that is used to handle/resolve errors.
*/

function showIntensityAlert(windSpeed) {
	try{
		//Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));

		// error/err are commonly used variable names used by the developers for storing errors.
	} catch (error) {
		// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is.
		console.log(typeof error);

		// catch errors within "try" statment
		// in this case the error is an unknown function "alert" which does not exist in Javascript
		//the 'alert' function is used similarly to a prompt to alert the user.
		// 'error.message' is used to access the information relating to an error object.
		console.log(error.message);

	} finally {
		// continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors.
		alert("Intensity updates will show new alert.");

	}
}

showIntensityAlert(56);

/*function gradeEvaluator(grade){
	try {
		if (grade >= 90) { 
			console.log ("Your grade is A!");
		} else if (grade >= 88){
			console.log ("Your grade is B!"+B);
		} else if (grade >= 71){
			console.log ("Your grade is C!");
		} else if (grade <= 70) {
			console.log ("Your grade is F!");
		}
	} catch (error) {
		console.log (typeof error);
		console.log (error.message);
	} finally {
		alert("Congratulations in your studies!");
	}
}

gradeEvaluator(89);*/

//Do While Loop
	//Do While Loop is similar to while loop. However, Do While loop will run the code block at least once before it checks the condition.

	//With While loop, we first check the condition and then run the code block/statements.

let doWhileCounter = 20

do{
	console.log(doWhileCounter);
	-- doWhileCounter
} while (doWhileCounter < 0);


// For loop
	// A for loop is more flexible then while and do-while loops.
	// It consists of 3 parts:
		// 1. The "intialization" value that will track the progression of the loop.
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		// 3. The "fixedExpression" indicated how to advance the loop.

/*
	Syntax:

	for(initialization, condition, finalExpression){

		//codeblock / statement
	}

*/

	//Create a loop that will start from 1 and end at 20

	for(let x = 1; x <= 20; x++){
		console.log(x);
	}

//Q: Can we use for loop in a string?
let name = "Nikko";

//Accessing elements of a string
	//using index
	//index starts at 0
console.log(name[0]);	//N
console.log(name[1]);	//i
console.log(name[2]);	//k

//Count of the string characters
	//using .length
console.log(name.length);

//Use for loop to display each character of the string
for(let index = 0; index < name.length; index++){
	console.log(name[index]);
}

//Using for loop in an array:
let fruits = ["mango", "apple", "orange", "banana", "strawberry", "kiwi"]
	//elements - each value inside the array
console.log(fruits);

//Total count of elements in an array
console.log(fruits.length);

//Access each element in an array
console.log(fruits[0]) //mango
console.log(fruits[4]) //strawberry
console.log(fruits[5]) //kiwi

//How do we determine the last element in an array if we don't the total number of the elements.
console.log(fruits[fruits.length - 1]);
// console.log(fruits[6])

//If we want to assign new value an element
fruits[6] = "grapes";
console.log(fruits);

//Using for loop in an array
for(let i = 0; i < fruits.length; i++){
	console.log(fruits[i]);
}

//Example of Array of objects
let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}
];

console.log(cars.length);	//3 elements

console.log(cars[1]);


// Use for loop to display each element in the cars array

//Mini-Activity:
for (let i = 0; i < cars.length; i++){
	console.log(cars[i]);
};

/*
let i = 0
for (i; i < 10; i++){}
*/

let myName = "adrIAn madArang"

for(let i = 0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
};

//Continue and Break
	//The "Continue" statement allows the code to go to the next iteration of the loop "without finishing the execution of the following statements in a code block."
		//- skips the current loop and continue to the next loop.


	for(let a = 20; a > 0; a--){

		if(a % 2 === 0){
			continue
		}

		console.log(a)

		if(a < 10){
			break
		}
	}


	//Mini-Activity:
	let name1 = "Bargaindaur"

	for (let i = 0; i < name1.length; i++){
		if (name1[i].toLowerCase() == "a"){
			continue;
		} 

		console.log(name1[i]);

		if (name1[i].toLowerCase() == "d"){
			break;
		}
	}

//Q: Is it possible to have another loop inside a loop? - Yes
//Nested Loops

for(let x = 0; x <= 10; x++){
	console.log(x);

	for(let y = 0; y <= 10; y++){
		console.log(`${x} + ${y} = ${x+y}`);
	}
}